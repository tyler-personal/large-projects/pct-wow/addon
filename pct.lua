-- VARIABLES --
local frame = CreateFrame("FRAME", "HelloWorldAddonFrame");

spellIDs = {}
spellNames = {}

json = '{"spells": ['

keybindsBySpellName = {}

-- VARIABLES END --

-- HELPER FUNCTIONS --
local function tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

local function has_value (tab, val)
    for index, value in ipairs(tab) do
        if value == val then
            return true
        end
    end
    return false
end

local function loadKeybindings()
  for i=1,400 do
    local type, globalID, subType = GetActionInfo(i)
    local name, rank, icon, castTime, minRange, maxRange, spellID = GetSpellInfo(globalID)
    
    -- This is hacky as hell. Gotta love the API
    local num = 0
    if (i <= 12) then 
      num = 27
    elseif (i <= 48) then
      num = 73
    elseif (i <= 60) then
      num = 35
    elseif (i <= 72) then
      num = 10
    else
      num = 100000
    end

    local command, _, keybind = GetBinding(i + num)
    
    if not (name == nil or keybind == nil) then
      keybindsBySpellName[name] = keybind
    end 
    print(name)
  end
end

function trackSpell(self, event, ...)
  if event == "UNIT_SPELLCAST_SUCCEEDED" then
    local unit, name, rank, lineID, spellID = ...
    local keybind = keybindsBySpellName[name]

    if not has_value(spellNames, name) then
      tinsert(spellIDs, spellID)
      tinsert(spellNames, name)
      
      print("PCT TRACKED: " .. name)
      
      C_Timer.After(.1, function()
        local startTime, duration, enabled = GetSpellCooldown(name)
        json = json .. '{"name": "' .. name .. '", "id": "' .. spellID .. '", "cooldown": "' .. duration .. '", "keybinding": "' .. keybind .. '"}, '
      end)
    else
      print("PCT already tracked that spell. Type \"/pct stop\" to stop.")
    end
  end
end
-- HELPER FUNCTIONS END --

-- MAIN --

loadKeybindings()

frame:SetScript("OnEvent", trackSpell);

SLASH_PCT1 = "/pct"
SlashCmdList["PCT"] = function(msg)
  if msg == 'start' then
    loadKeybindings()
    local ready = tablelength(keybindsBySpellName) > 0
    if ready then 
      frame:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED");
      print("PCT is now listening. Pull the cooldowns you want to track.")
      print("Then use \"/pct stop\" when finished.")
    else
      print("Couldn't start PCT.")
      print("This is a common issue if it's your first time logging in after installing the addon.")
      print("Type \"/reload\".")
    end
  elseif msg == 'stop' then
    frame:UnregisterEvent("UNIT_SPELLCAST_SUCCEEDED");
    print("Stopped listening. Type \"/reload\" to finalize. Then use the desktop app to push to the web.")
  elseif msg == 'delete' then
    spellIDs = {}
    spellNames = {}
    json = '{"spells": ['
    print("Information deleted.")
  elseif msg == 'help' then
    print("Valid options are: \"/pct start\" or \"/pct stop\" or \"/pct delete\"")
  else
    print("Not a valid command. Type \"/pct help\"")
  end
end 



